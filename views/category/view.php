<?php
/** @var array $category */
/** @var array $products */

use models\User;

?>
<h1><?= $category['name'] ?></h1>
<?php if (User::isAdmin()) : ?>
    <div class="mb-3">
        <a href="/product/add/<?=$category['id'] ?> " class="btn btn-success">Додати товар</a>
    </div>
<?php endif; ?>

<div class="row row-cols-1 row-cols-md-4 g-4 categories-list">
    <?php foreach ($products as $row) : ?>
        <div class="col">
            <a href="/product/view/<?= $row['id'] ?>" class="card-link">
                <div class="card">
                    <?php
                    $filePath = 'files/product/' . $row['photo'];
                    $defaultImagePath = '/static/images/2.jpg';
                    ?>
                    <img src="<?= is_file($filePath) ? '/' . $filePath : $defaultImagePath ?>" class="card-img-top" alt="">
                    <div class="card-body">
                        <h5 class="card-title"><?= $row['name'] ?></h5>
                    </div>
                    <div class="card-body">
                        <a href="/category/edit/<?= $row['id'] ?>" class="btn btn-primary">Редагувати</a>
                        <a href="/category/delete/<?= $row['id'] ?>" class="btn btn-danger">Видалити</a>
                    </div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>

</div>
