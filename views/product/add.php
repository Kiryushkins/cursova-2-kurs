<?php
/** @var array $model */
/** @var array $error */
/** @var array $categories */
/** @var int|null $category_id */

?>
<h2>Додавання товару</h2>
<form action="" method="post" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="name" class="form-label">Назва товару</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="">
        <?php if (!empty($error['name'])): ?>
            <div class ="form-text text-danger"> <?= $error['name']; ?></div>
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="name" class="form-label">Виберіть категорію товару</label>
        <select class="form-control" id="category_id" name="category_id" placeholder="">
            <?php foreach ($categories as $category) : ?>
                <option <?php if($category['id'] == $category_id) echo 'selected'; ?> value="<?=$category['id'] ?>"> <?=$category['name']?> </option>
            <?php endforeach; ?>
        </select>
        <?php if (!empty($error['category_id'])): ?>
            <div class ="form-text text-danger"> <?= $error['category_id']; ?></div>
        <?php endif; ?>
    </div>

    <div class="mb-3">
        <label for="name" class="form-label">Ціна товару</label>
        <input type="number" class="form-control" id="price" name="price" placeholder="">
        <?php if (!empty($error['price'])): ?>
            <div class ="form-text text-danger"> <?= $error['price']; ?></div>
        <?php endif; ?>
    </div>

    <div class="mb-3">
        <label for="name" class="form-label">кількість товару</label>
        <input type="number" class="form-control" id="count" name="count" placeholder="">
        <?php if (!empty($error['count'])): ?>
            <div class ="form-text text-danger"> <?= $error['count']; ?></div>
        <?php endif; ?>
    </div>

    <div class="mb-3">
        <label for="name" class="form-label">Короткий опис товару</label>
        <textarea class=" ckeditor form-control" name="short_description" id="short_description"> </textarea>
        <?php if (!empty($error['short_description'])): ?>
            <div class ="form-text text-danger"> <?= $error['short_description']; ?></div>
        <?php endif; ?>
    </div>

    <div class="mb-3">
        <label for="name" class=" form-label">Повний опис товару</label>
        <textarea class=" ckeditor form-control" name="description" id="description"> </textarea>
        <?php if (!empty($error['short_description'])): ?>
            <div class ="form-text text-danger"> <?= $error['description']; ?></div>
        <?php endif; ?>
    </div>

    <div class="mb-3">
        <label for="name" class="form-label">Відображати товар</label>
        <select class="form-control" id="visible" name="visible" placeholder="">
            <option value="1"> так </option>
            <option value="0"> ні </option>
        </select>
        <?php if (!empty($error['visible'])): ?>
            <div class ="form-text text-danger"> <?= $error['visible']; ?></div>
        <?php endif; ?>
    </div>


    <div class="mb-3">
        <label for="file" class="form-label">Файл з фотографією для категорії (замінити фото)</label>
        <input type="file" class="form-control" name="file" id="file" accept="image/jpeg" />
    </div>
    <div>
        <button class="btn btn-primary">Додати</button>
    </div>
</form>