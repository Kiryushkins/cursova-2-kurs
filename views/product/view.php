<?php
/** @var array $product */
?>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

<h1 class="h3 mb-3 fw-normal text-center"><?= $product['name'] ?> </h1>

<div class="container">
    <div class="row">
        <div class="col-6">

            <?php $filePath = 'files/product/' . $product['photo']; ?>
            <?php if (is_file($filePath)) : ?>
                <img src="/<?= $filePath ?>" class="img-thumbnail" alt="">
            <?php else: ?>
                <img src="/static/images/2.jpg" class="img-thumbnail" alt="">
            <?php endif; ?>
        </div>
        <div class="col-6">
            <div class="container">
                <div class="row mb-3">
                    <div class="col-4">
                        Ціна товару:
                    </div>

                    <div class="col-8">
                        <strong><?= $product['price'] ?> грн. </strong>
                    </div>
                </div>


                <div class="row mb-3">
                    <div class="col-4">
                        Доступна кількість:
                    </div>
                    <div class="col-8">
                        <strong><?= $product['count'] ?> шт. </strong>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-4">
                        Короткий опис:
                    </div>
                    <div class="col-8">
                        <?= $product['short_description'] ?>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-4">
                        Придбати:
                    </div>
                    <div class="col-3">
                        <input value="1" min="1" max="<?= $product['count'] ?>" type="number" name="count" id="count" class="form-control">
                    </div>

                    <div>
                        <button id="buyButton" class="btn btn-primary">Придбати</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#buyButton").click(function () {
            // Отримайте значення поля вводу кількості товару
            var quantity = $("#count").val();

            // Зробіть AJAX-запит на сервер для додавання товару до кошика
            $.ajax({
                type: "POST",
                url: "/add-to-basket", // Замініть це на шлях до вашого методу додавання товару до кошика
                data: {
                    productId: <?= $product['id'] ?>, // Передайте ідентифікатор товару
                    quantity: quantity,
                },
                success: function () {
                    // Перенаправте користувача на сторінку кошика
                    window.location.href = "/basket";
                },
                error: function () {
                    alert("Помилка додавання товару до кошика");
                },
            });
        });
    });
</script>