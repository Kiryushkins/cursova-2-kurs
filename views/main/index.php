<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Інтернет-магазин комп'ютерів</title>
    <style>
        /* Решта стилів тут залишається незмінною */

        .banner-container {
            position: relative;
            overflow: hidden;
            width: 100%;
            max-height: 400px; /* Максимальна висота банера */
            border-radius: 20px;
        }

        .banner-inner {
            display: flex;
            transition: transform 0.5s ease-in-out;
        }

        .banner-inner img {
            flex: 1;
            object-fit: cover;
            width: 100%;
            height: 100%;
            margin-right: 10px; /* Додайте бажаний інтервал сюди */
        }
        .office-container {
            display: flex;
            justify-content: space-between;
            margin-top: 20px; /* Додайте бажаний інтервал між картинками тут */
        }

        .office-container img {
            width: 48%; /* Змініть ширину картинок відповідно до ваших потреб */
            border-radius: 10px;
        }
        /* Огляд продукції */
        .overview {
            background-color: mediumseagreen;
            padding: 20px;
            border-radius: 10px;
            margin-bottom: 20px;
        }

        .overview h2 {

            font-size: 24px;
            margin-bottom: 10px;
        }

        .overview p {

            font-size: 16px;
        }

        /* Новинки та Рекомендації */
        .highlights {
            background-color: mediumseagreen;
            padding: 20px;
            border-radius: 10px;
            margin-bottom: 20px;
        }

        .highlights h2 {

            font-size: 24px;
            margin-bottom: 10px;
        }

        .highlights p {

            font-size: 16px;
        }

        /* Наш офіс та оранжерея в Києві */
        .office {
            background-color: mediumseagreen;
            padding: 20px;
            border-radius: 10px;
            margin-bottom: 20px;
        }

        .office h2 {

            font-size: 24px;
            margin-bottom: 10px;
        }

        .office p {

            font-size: 16px;
        }

        .office p:last-child {
            margin-bottom: 0;
        }
        .laskavo
        {
            border-radius: 10px;
            border: 10px solid white;
            color: white;
            background-color: rgba(0, 128, 0, 0.27); /* Зелений колір з прозорістю 27% */
        }



        /* Решта стилів тут залишається незмінною */
    </style>
</head>
<body>
<header class ="laskavo">
    <center> <h1>Ласкаво просимо в "PC shop"</h1> </center>
    <center> <p>Ваш помічник у світі комп'ютерів та ноутбуків</p> </center>
</header>

<section class="banner-container">
    <div class="banner-inner">
        <img src="/files/category/baner1.jpg" alt="Банер 1">
        <img src="/files/category/baner2.jpg" alt="Банер 2">
        <img src="/files/category/baner3.jpg" alt="Банер 3">
    </div>
</section>
<br></br>
<section class="overview">
    <h2>Огляд продукції</h2>
    <p>Відкрийте для себе усе необхідне для вашого робочого місця: ноутбуки, інструменти та чудові подарунки.</p>
</section>

<section class="highlights">
    <h2>Новинки та Рекомендації</h2>
    <p>Дізнайтеся про наші останні новинки та отримайте рекомендації для комфортного застосування ПК.</p>
</section>

<section class="office">
    <h2>Наш офіс в Києві</h2>
    <p>Знаходьте нас за адресою: вул. Слави, 1, Київ</p>
</section>

<section>
    <div class="office-container">
        <img src="/files/category/ofic.jpg" alt="Офіс">
        <img src="/files/category/ms.jpg" alt="Майстерня">
    </div>
</section>

<script>
    let currentSlide = 0;
    const totalSlides = document.querySelectorAll('.banner-inner img').length;

    function showSlide(index) {
        const offset = -index * 100;
        document.querySelector('.banner-inner').style.transform = `translateX(${offset}%)`;
    }

    function nextSlide() {
        currentSlide = (currentSlide + 1) % totalSlides;
        showSlide(currentSlide);
        setTimeout(nextSlide, 3000); // Таймаут між слайдами (3000 мілісекунд)
    }

    function prevSlide() {
        currentSlide = (currentSlide - 1 + totalSlides) % totalSlides;
        showSlide(currentSlide);
        setTimeout(nextSlide, 3000); // Таймаут між слайдами (3000 мілісекунд)
    }

    // Початкове відображення першого слайда
    showSlide(currentSlide);

    // Запускаємо автоматичну зміну слайдів
    setTimeout(nextSlide, 3000); // Початковий таймаут (3000 мілісекунд)
</script>

</body>
</html>